﻿using System;
using System.Collections.Generic;

namespace ExpandableListViewDemo
{
//public class Subcatery
//{
//	public string a { get; set; }
//	public string b { get; set; }
//	public string c { get; set; }
//	public string d { get; set; }
//	public string e { get; set; }
//}
//
//public class ListData
//{
//	public string GroupName { get; set; }
//	public List<Subcatery> Subcatery { get; set; }
//}
//
//public class DataList
//{
//	public List<ListData> ListData { get; set; }
//}
//}

public class ListData
{
	public string GroupName { get; set; }
	public List<string> Subcatery { get; set; }
}

public class DataList
{
	public List<ListData> ListData { get; set; }
}
}