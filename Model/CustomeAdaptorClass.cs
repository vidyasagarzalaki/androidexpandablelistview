﻿		using System;
		using System.Collections.Generic;
		using System.Linq;
		using System.Text;
		using Android.App;
		using Android.Content;
		using Android.OS;
		using Android.Runtime;
		using Android.Views;
		using Android.Widget;

		namespace ExpandableListViewDemo {
			public class CustomeAdaptorClass : BaseExpandableListAdapter {
		//readonly string[]str;
		readonly List<ListData> lstSubCategory;
				readonly Activity Context;
		public CustomeAdaptorClass(Activity newContext,List<ListData> lstSubCategory1) : base()
				{
					Context = newContext;
			//str = str1;
			lstSubCategory = lstSubCategory1;

				}

		//**********************************************    Group Get view  		**********************************************************

				public override View GetGroupView (int groupPosition, bool isExpanded, View convertView, ViewGroup parent)
				{
			
			View header = convertView;
			ViewHolderEvents objViewHolderEvents = new ViewHolderEvents ();
			if (header == null) {
				header = Context.LayoutInflater.Inflate (Resource.Layout.CustomGroupListLayout, null);
				objViewHolderEvents.lblGroupName = header.FindViewById<TextView> ( Resource.Id.txtGroupHeader );
				objViewHolderEvents.imgEventLogo=header.FindViewById<ImageView> ( Resource.Id.imageView1 );
				objViewHolderEvents.Initialize ( header );

				header.Tag = objViewHolderEvents;  
			}
			else
			{
				objViewHolderEvents = (ViewHolderEvents)header.Tag;
			}
			if (isExpanded)
			{
				objViewHolderEvents.lblGroupName.SetTypeface(null, Android.Graphics.TypefaceStyle.Bold);
				objViewHolderEvents.imgEventLogo.SetImageResource(Resource.Drawable.minus);
			}
			else 
			{
				objViewHolderEvents.lblGroupName.SetTypeface(null, Android.Graphics.TypefaceStyle.Normal);

				objViewHolderEvents.imgEventLogo.SetImageResource(Resource.Drawable.Plus);
			}
			objViewHolderEvents.lblGroupName.Text = lstSubCategory [groupPosition].GroupName;
			return header;
				}
		//**********************************************    Child Get view  		**********************************************************
				public override View GetChildView (int groupPosition, int childPosition, bool isLastChild, View convertView, ViewGroup parent)
				{
			View view = convertView;
			ViewHolderEvents objViewHolderEvents = new ViewHolderEvents ();
			if (view == null) 
					{
				view = this.Context.LayoutInflater.Inflate (Resource.Layout.CustomChildernLayout, null); 


				objViewHolderEvents.lblGroupName =view.FindViewById<TextView> ( Resource.Id.textView1 );
				objViewHolderEvents.Initialize ( view );

				view.Tag = objViewHolderEvents;  

					}
			else
			{
				objViewHolderEvents = (ViewHolderEvents)view.Tag;
			}

//			foreach(var a in lstSubCategory [groupPosition].Subcatery)
//			{
//				objViewHolderEvents.lblGroupName.Text =a;
//			}
			objViewHolderEvents.lblGroupName.Text = lstSubCategory [groupPosition].Subcatery [childPosition];
			return view;
					//throw new NotImplementedException ();
				}

				public override int GetChildrenCount (int groupPosition)
				{
			var s=lstSubCategory [groupPosition].Subcatery.Count;
		
			return s;
				}

				public override int GroupCount
				{
			get { return this.lstSubCategory.Count(); }
				}


				private void GetChildViewHelper (int groupPosition, int childPosition)
				{
					
				}

				#region implemented abstract members of BaseExpandableListAdapter

				public override Java.Lang.Object GetChild (int groupPosition, int childPosition)
				{
					throw new NotImplementedException ();
				}

				public override long GetChildId (int groupPosition, int childPosition)
				{
			return groupPosition;
				}

				public override Java.Lang.Object GetGroup (int groupPosition)
				{
					throw new NotImplementedException ();
				}

				public override long GetGroupId (int groupPosition)
				{
					return groupPosition;
				}

				public override bool IsChildSelectable (int groupPosition, int childPosition)
				{
					//throw new NotImplementedException ();
			return true;
				}

				public override bool HasStableIds {
					get {
						return true;
					}
				}

				#endregion


			}
	public	class ViewHolderEvents : Java.Lang.Object
	{
		public ImageView imgEventLogo;
		public TextView lblGroupName;
		public TextView lblChildName;
		public Action ViewClicked{ get; set;}
		public void Initialize(View view)
		{
//			view.Click += delegate(object sender , EventArgs e )
//			{
//				ViewClicked();
//			};
	}
		}
}
