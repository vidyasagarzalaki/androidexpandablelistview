using Android.App;
using Android.Widget;
using Android.OS;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using Android.App;
using System;
using Android.Support.V7.App;

namespace ExpandableListViewDemo
{
	[Activity (Theme="@style/MyTheme", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : AppCompatActivity
	{
		int count = 1;
		ExpandableListView listView;
		DataList objDataList;
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			// Get our button from the layout resource,
			// and attach an event to it
			listView = FindViewById<ExpandableListView> (Resource.Id.expandableListView1);
			objDataList = objDataList ?? new DataList ();

			FnMenu ();
			listView.ChildClick += delegate(object sender , ExpandableListView.ChildClickEventArgs e )
			{
				//write code here
				Console.WriteLine(e.ChildPosition)	;
			};


		}
		void FnMenu()
		{

			//int[] strMnuUrl=new int[]{Resource.Drawable.home,Resource.Drawable.location_black,Resource.Drawable.orderdetails,Resource.Drawable.imgUsername_icon,Resource.Drawable.feedback,Resource.Drawable.logout}; 
			//reading file in assets folder
			using(StreamReader objStreamReader=new StreamReader(Assets.Open("EmptyXmlFile.json")))
			{
				string strRead =objStreamReader.ReadToEnd();
				objDataList = JsonConvert.DeserializeObject <DataList>( strRead );
			}

			List<ListData> lstSubCategory = new List<ListData> (objDataList.ListData);
			listView.SetAdapter ( new CustomeAdaptorClass ( this ,lstSubCategory) );
		}
	}
}
